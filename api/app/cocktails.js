const express = require('express');
const Cocktail = require('../models/Cocktail');
const User = require('../models/User');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middlewere/auth');
const permit = require('../middlewere/permit');
const tryAuth = require('../middlewere/tryAuth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/',tryAuth,async (req,res)=>{
    try {
        let criteria = {published: true};

        if(req.user){
            criteria = {
                $or:[
                    {published: true},
                    {user:req.user._id}
                ]
            }
        }

        const cocktails = await Cocktail.find(criteria);

        return res.send(cocktails);
    }catch (e) {
        console.log(e);
        return res.status(500).send(e);
    }

});
 
router.post('/' , auth ,upload.single('image') , async (req,res)=>{
    console.log(req.body);
    try {
        const ingredients= JSON.parse(req.body.ingredients);

        const cocktails = new Cocktail({
            title: req.body.title,
            user: req.user._id,
            image: req.file.filename,
            recipe: req.body.recipe,
            ingredients: ingredients
        });

        await cocktails.save();

        res.send(cocktails);
        
    }catch (e) {
        if(e.name === 'ValidationError'){
            return res.status(400).send(e);
        }
        return res.status(500).send(e);
    }
});

router.post('/:id/toggle_published', async (req, res)=>{
    console.log(req);

    const token = req.body.headers.Token;

    if(!token){
        return res.status(401).send({error:'Token not provided'})
    }

    const user = await User.findOne({token});

    if(!user){
        return res.status(401).send({error: 'Token incorrect'})
    }
    if(user.role !== 'admin'){
        return res.status(403).send({message: 'Unauthorized'})
    }

    const cocktails = await Cocktail.findById(req.params.id);

    if(!cocktails){
        return res.sendStatus(404);
    }

    cocktails.published = !cocktails.published;

    await cocktails.save();
    return res.send(cocktails);



});


router.delete('/:id', auth, async (req,res)=>{

    const cocktail = await Cocktail.findOne({
        _id: req.params.id,
        user: req.user._id
    });

    if(!cocktail){
        return res.sendStatus(403);
    }
    await cocktail.remove();
    res.send({message: "OK"})
});


module.exports =router;
