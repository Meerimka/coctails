const mongoose =require('mongoose');

const Schema = mongoose.Schema;

const IngSchema = new Schema({
    name: String,
    amount: {
        type: Number
    }

});

const CocktaiSchema = new Schema({
    title:{
        type:String,
        required: true,
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    },
    image: String,
    recipe: String,
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    ingredients:[IngSchema]
});

const  Cocktail = mongoose.model('Cocktail', CocktaiSchema);


module.exports = Cocktail;
