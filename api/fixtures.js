const mongoose = require('mongoose');
const nanoid =require('nanoid');
const Cocktail =require('./models/Cocktail');
const User =require('./models/User');
const config = require('./config');



const run =async () => {
  await  mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for(let collection of collections){
      await collection.drop();
  }

  const [user, admin] = await User.create({
    username: 'Nancy Alcgjgbhddcei Warmansky',
      password: '123456',
      role: 'user',
      displayName:'User',
      avatar: 'User.jpg',
      token: nanoid()
  },
      {
          username: 'admin',
          password: '123',
          role: 'admin',
          displayName:'Admin',
          facebookId:'1234811046686144',
          avatar: 'User.jpg',
          token: nanoid()
      });

const coctails = await Cocktail.create(
    {
        title: "Gin",
        image: "eminem.jpg",
        published: 'true',
        user: user._id,
        recipe: "Gin"
    },
    {
        title: "Rom",
        image: 'logic.jpg',
        published: 'false',
        user: user._id,
        recipe: 'Rom'
    },
    {
        title: "Martini",
        image: 'linkin.jpg',
        published: 'false',
        user: admin._id,
        recipe: 'Martini'
    },
);
  return connection.close();

};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});

