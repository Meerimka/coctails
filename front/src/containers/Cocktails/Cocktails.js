import React, {Component, Fragment} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import CocktailListItem from "../../components/ListItems/CocktailListItem";
import MusicThumbnail from "../../components/CocktailThumbnail/CocktailThumbnail";
import './Cocktails.css';
import {deleteCocktail, fetchCocktails, togglePublish} from "../../store/actions/cocktailActions";



class Cocktails extends Component {
    state={
        selectedItem:null
    };

    showModal = cocktails =>{
        this.setState({selectedItem: cocktails})
    };

    hideModal = () =>{
        this.setState({selectedItem: null})
    };

    componentDidMount() {
        this.props.fetchCocktails();
    }

    render() {
        const cocktails= this.props.cocktails.map(cocktail => (
            <CocktailListItem
                showModal ={()=>this.showModal(cocktail)}
                key={cocktail._id}
                _id={cocktail._id}
                title={cocktail.title}
                image={cocktail.image}
               published={cocktail.published}
                user={this.props.user}
                togglePublish={()=>this.props.togglePublish(cocktail._id)}
                deleteCocktail={()=>this.props.deleteCocktail(cocktail._id)}
            />

        ));
        return (
            <Fragment>
                <h2>
                    Cocktails
                </h2>
                <div className="Cocktails">
                    {cocktails}
                </div>

                <Modal isOpen={!!this.state.selectedItem} toggle={this.hideModal}>
                    {this.state.selectedItem && (
                        <Fragment>
                            <ModalHeader toggle={this.hideModal}>{this.state.selectedItem.title}</ModalHeader>
                            <ModalBody>
                                <MusicThumbnail className="img-thumbnail" image={this.state.selectedItem.image}/>
                                <div>
                                    <h3>Recipe:</h3>
                                    &nbsp;{this.state.selectedItem.recipe}<br/>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={this.hideModal}>Cancel</Button>
                            </ModalFooter>
                        </Fragment>
                    )}

                </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cocktails: state.cocktails.cocktails,
    user:state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchCocktails: () => dispatch(fetchCocktails()),
    togglePublish : (cocktailId) => dispatch(togglePublish(cocktailId)),
    deleteCocktail: (cocktailId)=>dispatch(deleteCocktail(cocktailId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);


