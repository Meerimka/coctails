import React, {Component} from 'react';
import {connect} from "react-redux";
import {Col, Button, Form, FormGroup, Input, Label, Row} from "reactstrap";
import {createCocktail} from "../../store/actions/cocktailActions";



class NewCocktails extends Component {

    state = {
        title: '',
        recipe: '',
        image: null,
        ingredients:[
            {name: '', amount: ''}
        ]
    };



    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            if(key === 'ingredients') {
                formData.append(key, JSON.stringify(this.state[key]))
            } else {
                formData.append(key, this.state[key])
            }
        });

        this.props.CocktailCreated(formData);
        console.log(this.state);
        this.props.history.push('/');

    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    addIngredients = event =>{
        event.preventDefault();

        this.setState({
            ingredients: [
            ...this.state.ingredients,
        {name: '', amount: ''}
        ]
        })
    };

    removeIngredients = idx =>{
    const ingredients = [...this.state.ingredients];
        ingredients.splice(idx,1);

        this.setState({ingredients})
    };

    ingredientChangeHandler = (event, idx) =>{
        const ingredient = {...this.state.ingredients[idx]};

        ingredient [event.target.name]= event.target.value;

        const ingredients = [...this.state.ingredients];

        ingredients[idx] = ingredient;

    this.setState({ingredients})
            };

    render() {
        return (
                <Form onSubmit={this.submitFormHandler}>
                    <h2>New Cocktail</h2>
                    <FormGroup row>
                        <Label sm={2} for="title">Title</Label>
                        <Col sm={10}>
                            <Input
                                type="text"
                                name="title" id="title"
                                placeholder="Enter product title"
                                value={this.state.title}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                    <Label sm={2} for="ingredients" >Ingredients</Label>
                        <Col sm={10}>
                    {this.state.ingredients.map((ing,idx)=>(
                        <FormGroup key={idx} row>
                            <Col sm={8}>
                        <Input type="text" name="name" id="name"  placeholder="Enter ingredients"
                               onChange={(event)=>this.ingredientChangeHandler(event, idx)}/>
                            </Col>
                            <Col sm={2}>
                        <Input type="number" name="amount" id="amount" placeholder=" amount"
                             onChange={(event)=>this.ingredientChangeHandler(event, idx)}/>
                            </Col>
                            {idx > 0 && <Button type="button" color= "danger"  onClick={()=>this.removeIngredients(idx)} sm={2}>X</Button>}
                        </FormGroup>
                    ))}
                            <Button type="button" onClick={this.addIngredients}>Add ingredients</Button>
                        </Col>

                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="recipe">Recipe</Label>
                        <Col sm={10}>
                            <Input
                                type="textarea"
                                name="recipe" id="recipe"
                                placeholder="Enter  cocktails recipe"
                                value={this.state.recipe}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="image">Cocktail image</Label>
                        <Col sm={10}>
                            <Input
                                type="file"
                                name="image" id="image"
                                onChange={this.fileChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset:2, size: 10}}>
                            <Button type="submit" color="primary">Create Cocktail</Button>
                        </Col>
                    </FormGroup>
                </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    CocktailCreated: cocktailData => dispatch(createCocktail(cocktailData))
});

export default connect(null, mapDispatchToProps)(NewCocktails);