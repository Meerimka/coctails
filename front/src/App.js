import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Container} from "reactstrap";
import {Switch, Route, withRouter} from "react-router-dom";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {logoutUser} from "./store/actions/usersActions";
import FacebookLogin from "./components/FacebookLogin";
import NewCocktails from "./containers/Cocktails/NewCocktails";
import Cocktails from "./containers/Cocktails/Cocktails";
import {NotificationContainer} from "react-notifications";



class App extends Component {
    render() {
        return (
            <Fragment>
                <NotificationContainer/>
            <header>
            <Toolbar user={this.props.user}  logout={this.props.logoutUser}/>
        </header>
        <Container style={{marginTop: '20px'}}>
    <Switch >
        <Route path="/" exact component={Cocktails}/>
        <Route path="/cocktails/new" exact component={NewCocktails}/>
        <Route path="/register" exact component={FacebookLogin}/>
        <Route path="/login" exact component={FacebookLogin}/>
        </Switch>
        </Container>
        </Fragment>
    );
    }
}

const mapStateToProps = state =>({
    user: state.users.user
});
const mapDispatchToProps = dispatch =>({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(App));


