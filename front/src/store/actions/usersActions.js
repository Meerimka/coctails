import {push} from 'connected-react-router';
import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';

export const LOGOUT_USER= 'LOGOUT_USER';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';


const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});
const logOutUser = () => ({type: LOGOUT_USER});



export const logoutUser = () =>{
    return (dispatch, getState) =>{
        const token = getState().users.user.token;
        const config = {headers: {'Token': token}};
        return axios.delete('/users/sessions', config).then(
            () =>{
                dispatch(logOutUser());
                dispatch(push('/'));
                NotificationManager.success('logged out')
            },
            error =>{
                NotificationManager.error('Could not logout');
            }
        )
    }
};

export const  facebookLogin = userData =>{
    return dispatch => {
        return axios.post('/users/facebookLogin', userData).then(
            response => {
                console.log(response);
                dispatch(loginUserSuccess(response.data.user));
                NotificationManager.success('Logged in via Facebook');
                dispatch(push('/'));
            },
            ()=>{
                dispatch(loginUserFailure('Login via Facebook failed'))
            }
        )
    }
};

