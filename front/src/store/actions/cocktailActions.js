import axios from '../../axios-api';
import {push} from "connected-react-router";
import {NotificationManager} from 'react-notifications';


export const FETCH_COCKTAIL_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const CREATE_COCKTAIL_SUCCESS = 'CREATE_ARTIST_SUCCESS';
export const TOGGLE_COCKTAIL_SUCCESS = 'TOGGLE_COCKTAIL_SUCCESS';
export const FETCH_FAIL = 'FETCH_FAIL';

export const fetchCocktailSuccess = cocktails => ({type: FETCH_COCKTAIL_SUCCESS, cocktails});
export const createCocktailSuccess = () => ({type: CREATE_COCKTAIL_SUCCESS});
export const toggleCocktailSuccess = () => ({type: TOGGLE_COCKTAIL_SUCCESS});
export const fetchFail = error => ({type: FETCH_FAIL, error});

export const fetchCocktails = () => {
    return (dispatch , getState)=> {
        const user = getState().users.user;
        if(user === null){
            return axios.get('/cocktails').then(
                response => {
                    console.log(response);
                    dispatch(fetchCocktailSuccess(response.data))
                }
            );
        }else {
            return axios.get('/cocktails',{headers: {'Token': user.token}}).then(
                response => {
                    console.log(response);
                    dispatch(fetchCocktailSuccess(response.data))
                }
            );
        }
    };
};

export const createCocktail= cocktailData => {
    return (dispatch, getState) => {

        const user = getState().users.user;
        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.post('/cocktails', cocktailData, {headers: {'Token': user.token}}).then(
                () => {

                    dispatch(createCocktailSuccess());
                    dispatch(fetchCocktails());
                   NotificationManager.success('Your cocktails in moderate state');
                }
            );
        }

    };
};


export const togglePublish = cocktailId => {
    return (dispatch, getState) => {

        const user = getState().users.user;
        if(user === null && user.role !== "admin"){

            dispatch(push('/login'));
        }else{
            console.log(user.token);
            return axios.post(`/cocktails/${cocktailId}/toggle_published`,{headers: {'Token': user.token}}).then(
                () => {
                    dispatch(toggleCocktailSuccess());
                    dispatch(fetchCocktails());
                }
            );
        };

    };
};

export const deleteCocktail = (cocktailId) => {
    return (dispatch,getState) => {
        const user = getState().users.user;

        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.delete('/cocktails/' + cocktailId, {headers: {'Token': user.token}}).then(
                () => {
                    dispatch(fetchCocktails());
                    NotificationManager.success('Deleted cocktail  successfully');
                },
                error => dispatch(fetchFail(error))
            );
        }

    }
};



