import {FETCH_COCKTAIL_SUCCESS} from "../actions/cocktailActions";


const initialState = {
    cocktails: [],
};

const cocktailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COCKTAIL_SUCCESS:
            return {...state, cocktails: action.cocktails};
        default:
            return state;
    }
};

export default cocktailsReducer;