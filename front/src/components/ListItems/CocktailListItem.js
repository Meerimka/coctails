import React from 'react';
import PropTypes from 'prop-types';
import {Button, ButtonGroup, Card, CardBody} from "reactstrap";

import {Link} from "react-router-dom";
import '../../containers/Cocktails/Cocktails.css';
import CocktailThumbnail from "../CocktailThumbnail/CocktailThumbnail";

const CocktailListItem = props => {
    return (
        <Card className="Cocktail-card" style={{'marginTop': '10px', cursor: "pointer"}} >
            <CardBody>
                <div onClick={()=>props.showModal(props)} >
                    <CocktailThumbnail image={props.image} />
                </div>

                <h2>
                <Link to={'/albums/' + props._id}>
                    {props.title}
                </Link>
                </h2>

                {props.user && props.user.role === "admin" &&
                    <ButtonGroup>
                        <Button color="success" onClick={props.togglePublish}>
                            {props.published === false ? 'Unpublished' : 'Publish'}
                        </Button>
                        <Button color="danger" onClick={props.deleteCocktail}>Delete</Button>
                    </ButtonGroup>
                }
            </CardBody>
        </Card>
    );
};

CocktailListItem.propTypes ={
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string,
    published:PropTypes.bool,
    showModal: PropTypes.func.isRequired,
    togglePublish:PropTypes.func.isRequired,
    deleteCocktail:PropTypes.func.isRequired

};
export default CocktailListItem;